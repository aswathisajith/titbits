# Version Control

### What is
Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later.

### Approaches
##### Centralized Version Control Systems
- have a single server that contains all the versioned files, and a number of clients that check out files from that central place
- client needs to check out/in files
- Central server is single point of failure - nobody can check out/in if the server goes down
- Example: CVS, Subversion  

##### Distributed Version Control Systems
- clients don’t just check out the latest snapshot of the files; rather, they fully mirror the repository, including its full history.
- do local commits, which are pushed to remote server
- Example : Git, Mercurial

##### Comparison

[What is version control: centralized vs. DVCS](https://www.atlassian.com/blog/software-teams/version-control-centralized-dvcs)

## Reference
- [Version Control with Subversion](http://svnbook.red-bean.com/)
- [Getting Started - About Version Control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)
- [Atlassian - What is version control](https://www.atlassian.com/git/tutorials/what-is-version-control) 
