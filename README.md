# Techno-titbits
*Short notes on technology*

## Basic

- [Networking](https://bitbucket.org/aswathisajith/titbits/src/master/L01N01-Networking.md)
- [Markdown](https://bitbucket.org/aswathisajith/titbits/src/master/L01N02-Markdown.md)
- [Version Control](https://bitbucket.org/aswathisajith/titbits/src/master/VersionControl)

## Intermediate

- [Git]()

## Advanced

- [PL/SQL Subprograms](https://bitbucket.org/aswathisajith/titbits/src/master/L03N01-PLSQL_Subprograms.md)
