# Markdown
**Markdown** is a lightweight markup language with plain text formatting syntax

## Examples
---
# H1
## H2
### H3
---
**bold text**
---
*italicized text*
---
> blockquote
---
1. First item
2. Second item
3. Third item
---
- First item
- Second item
- Third item
---
`code`
---
[Wikipedia](https://en.wikipedia.org/wiki/Markdown)
---
![Markdown Image alt-text](https://en.wikipedia.org/wiki/File:Markdown-mark.svg)

## Reference
* [Basic Syntax](https://www.markdownguide.org/basic-syntax/)
